<!DOCTYPE html>
<html lang="ar" dir="rtl" >
<style>
    body {
        font-family: 'XB Riyaz', sans-serif;
    }
</style>
<head>
    <meta charset="UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Document</title>
    <style>
    body {
        font-family: 'XB Riyaz', sans-serif;
       
    }
        </style>
</head>

<body>
<div class="head-letter-image"></div>
 

<div class="head-letter-text" style="width:50%;" >
<img
        src="images/logo svg.svg"
        alt=""
        srcset=""
        style="height: 8rem;float:left;     width:4rem;"
      />
    
      
    <h4>سلطنة عمان</h4>
    <h4>وزارة التربية والتعليم</h4>
    <h4>المديرية العامة للتربية والتعليم</h4>
    <h4>المحافظة</h4>
    <h4>المدرسة</h4>
</div>
</div>
 
    <div
        class="title_let"
        style="
          display: flex;
          justify-content: center;
          margin-top: -1rem;
          margin-bottom: -2rem;
        "
    >
        <h4 style="padding: 1rem 17rem;     border-radius: 50%; border: 1px solid;">استمارة فحص طبي</h4>
    </div>
    <h4>الفاضل/الطبيب المسؤول : ___________ ____________ ____________ المحترم __________</h4>
    <h4 style="margin-bottom: -1rem"></h4>

    <div class="title_let" style="display: flex; justify-content: center">
        <h4 style="margin-bottom: -1rem;padding: 1rem 12rem;">
            السلام عليكم ورحمة الله وبركاته وبعد..
        </h4>
    </div>
    <h4>يرجى التكرم باجراء الفحص الطبي على</h4>
   
        <h4> الطالب/التلميذ {{$entry->first_name}} {{$entry->last_name}} </h4>
        <h4> الجنسية {{$entry->nationality}}  تاريخ الميلاد  {{$entry->date_of_birth}}</h4>
        <h4> الولاية/العنوان __________ القرية __________</h4>
        
   
    <h4 style="margin-bottom: -0.5rem">اسم ولي الأمر {{$entry->guardian_first_name }} {{ $entry->guardian_last_name}}</h4>
    <div style="display: flex; margin-bottom: -1.5rem">
        <h4>عنوانه {{$entry->address}} رقم الهاتف {{$entry->guardian_phone}}</h4>
     </div>
    <div
        class="title_let"
        style="display: flex; "
    >
        <h4 style="padding: 2rem 0rem 0.5rem "> توقيع مدير المدرسة<img  src="images/sig.png " style="height:8rem;margin-right:-8rem;        " />ختم المدرسة<img  src="images/kh.png" style="height:4rem;margin-right:-5rem;margin-bottom:2rem;" /> </h4>
       
    </div>
     
    <div style="display: flex; margin-bottom: -18px;margin-top:1rem;">
        <h4> تم الكشف عن الطالب/ التلميذ المذكور اعلاه بتاريخ     /        /     ووجد انه       _______  </h4>
     </div>
    <h4>لايعاني من اي مشاكل صحية ---------</h4>
    <h4> يعاني من مشاكل صحية المتمثلة في الآتي---------</h4>
    <hr />
    <h4>توصية الطبيب</h4>
    <hr />
    <h4>اسم الطبيب ---------   توقيع وختم الطبيب ---------</h4>
     
</div>
</body>
</html>

