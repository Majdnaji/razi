
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>استمارة تسجيل</title>
    <!-- Mobile Specific Metas -->

    <meta
        name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1"
    />
    <!-- Font-->
    <link rel="stylesheet" type="text/css" href="{{asset('css/opensans-font.css')}}" />
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
    <link
        rel="stylesheet"
        type="text/css"
        href="{{asset('fonts/material-design-iconic-font/css/material-design-iconic-font.min.css')}}"
    />
    <!-- Main Style Css -->
    <link rel="stylesheet" href="{{asset('css/style.css')}}" />
    <style>

        .inner{

            /* height: 39rem; */
        }
        .inner .form-row{
            margin: -11px -10px !important;
        }
    </style>
</head>

<body >
<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
<script src="{{asset('js/jquery.steps.js')}}"></script>
<script src="{{asset('js/main.js')}}"></script>
<div class="page-content"  >

    <div class="form-v1-content">

        <div class="wizard-form">
            <form class="form-register" action="api/store" method="POST" accept-charset="UTF-8" enctype="multipart/form-data">


                <div id="form-total">
                    <!-- SECTION 1 -->
                    <h2>
                        <p class="step-icon"><span>01</span></p>
                        <span class="step-text">بيانات الطالب</span>
                    </h2>
                    <section>
                        <div class="inner"  dir="rtl">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                بيانات الطالب
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>اسم الطالب</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="first-name"
                                            name="first_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>الكنية</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="last-name"
                                            name="last-name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>مكان الميلاد</legend>
                                        <input
                                            type="text"
                                            name="birth_place"
                                            id="birth_place"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>المنطقة السكنية</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="area"
                                            name="area"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>رقم الجواز</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="passport_number"
                                            name="passport_number"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="nationality"
                                        name="nationality"
                                        placeholder="الجنسية"
                                        required
                                    />
                                </div>
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="civil_number"
                                        name="civil_number"
                                        placeholder="الرقم المدني"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="form-row form-row-date" >
                                <div class="form-holder form-holder-2">
                                    <label class="special-label" for="Birth">تاريخ الميلاد</label>
                                    <input type="date" name="date_of_birth" id="date_of_birth">
                                </div>
                            </div>
                            <div class="form-row form-row-date">
                                <div class="form-holder form-holder-2">
                                    <label class="special-label">تاريخ الاصدار</label>
                                    <input type="date" name="issue_date" id="issue_date">

                                </div>





                    </section>
                    <!-- SECTION 2 -->
                    <h2>
                        <p class="step-icon"><span>02</span></p>
                        <span class="step-text">بيانات ولي الأمر</span>
                    </h2>
                    <section>
                        <div class="inner" dir="rtl">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                بيانات ولي الأمر
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>اسم ولي الأمر</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="guardian_first_name"
                                            name="guardian_first_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>الكنية</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="guardian_last_name"
                                            name="last-name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>الجنسية</legend>
                                        <input
                                            type="text"
                                            name="guardian_nationality"
                                            id="guardian_nationality"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>درجة القرابة</legend>
                                        <input
                                            type="text"
                                            name="relativity"
                                            id="relativity"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>الرمز البريدي</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="post_code"
                                            name="post_code"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>صندوق البريد</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="post_box"
                                            name="post_box"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="guardian_educational_degree"
                                        name="guardian_educational_degree"
                                        placeholder="المستوى التعليمي"
                                        required
                                    />
                                </div>
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="address"
                                        name="address"
                                        placeholder="العنوان"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="guardian_job"
                                        name="guardian_job"
                                        placeholder="الوظيفة"
                                        required
                                    />
                                </div></div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="guardian_phone"
                                        name="guardian_phone"
                                        placeholder="رقم الهاتف"
                                        required
                                    />
                                </div>

                    </section>
                    <h2>
                        <p class="step-icon"><span>03</span></p>
                        <span class="step-text">بيانات اجتماعية</span>
                    </h2>
                    <section>
                        <div class="inner" dir="rtl">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                بيانات اجتماعية
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>عدد الأسرة</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="family_number"
                                            name="family_number"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>مستوى المعيشة</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="life_status"
                                            name="life_status"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">

                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>عدد الأخوة البنات</legend>
                                        <input
                                            type="text"
                                            name="sisters_number"
                                            id="sisters_number"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>عدد الأخوة البنين</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="brothers_number"
                                            name="brothers_number"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend> الترتيب بين الأخوة</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="sort_of_family"
                                            name="sort_of_family"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="num_of_brothers_on_school"
                                        name="num_of_brothers_on_school"
                                        placeholder="عدد الأخوة بالمدرسة"
                                        required
                                    />
                                </div>
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="stu_live_with"
                                        name="stu_live_with"
                                        placeholder="مع من يعيش الطالب"
                                        required
                                    />
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="how_to_go"
                                        name="how_to_go"
                                        placeholder="وسيلة ذهاب الطالب الى المدرسة"
                                        required
                                    />
                                </div></div>


                    </section>
                    <h2>
                        <p class="step-icon"><span>04</span></p>
                        <span class="step-text">بيانات الأم</span>
                    </h2>
                    <section>
                        <div class="inner" style="height: 39rem;" dir="rtl">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                  بيانات الأم
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>اسم الام </legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="mother_first_name"
                                            name="mother_first_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>الكنية</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="mother_last_name"
                                            name="mother_last_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>المستوى التعليمي </legend>
                                        <input
                                            type="text"
                                            name="mother_educational_degree"
                                            id="mother_educational_degree"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend> الوظيفة</legend>
                                        <input
                                            type="text"
                                            name="mother_job"
                                            id="mother_job"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend> مقر العمل</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="mother_work_address"
                                            name="mother_work_address"

                                            required
                                        />
                                    </fieldset>
                                </div>

                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <input
                                        type="text"
                                        class="form-control input-border"
                                        id="mother_phone"
                                        name="mother_phone"
                                        placeholder="رقم الهاتف"
                                        required
                                    />
                                </div>


                    </section>
                    <h2>
                        <p class="step-icon"><span>05</span></p>
                        <span class="step-text">احد الاقارب</span>
                    </h2>
                    <section>
                        <div class="inner" style="    height: 39rem;" dir="rtl">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                احد الاقارب
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>الاسم</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="relative_first_name"
                                            name="relative_first_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder">
                                    <fieldset>
                                        <legend>الكنية</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="relative_last_name"
                                            name="relative_last_name"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">

                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>درجة القرابة</legend>
                                        <input
                                            type="text"
                                            name="relative_relativity"
                                            id="relative_relativity"
                                            class="form-control"

                                            required
                                        />
                                    </fieldset>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>رقم الهاتف</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="relative_phone"
                                            name="relative_phone"

                                            required
                                        />
                                    </fieldset>
                                </div>
                                <div class="form-holder form-holder-2">
                                    <fieldset>
                                        <legend>الرقم المدني</legend>
                                        <input
                                            type="text"
                                            class="form-control"
                                            id="relative_citizen_number"
                                            name="relative_citizen_number"

                                            required
                                        />
                                    </fieldset>
                                </div>


                    </section>
                    <h2>
                        <p class="step-icon"><span>06</span></p>
                        <span class="step-text">المستندات المطلوبة</span>
                    </h2>
                    <section>
                        <div class="inner" style="    height: 39rem;">
                            <div class="wizard-header" style="text-align: right">
                                <h3 class="heading" style="text-align: center !important">
                                  المستندات المطلوبة
                                </h3>
                                <img src="images/logo animated.svg" style="    height: 9rem;float: left;"/>
                                <h4>سلطنة عمان</h4>
                                <h4>وزارة التربيية والتعليم</h4>
                                <h4>مدرسة الرازي الخاصة</h4>
                            </div>
                            <div class="form-row">
                                <div class="form-holder">

                                    <p >الرجاء ارفاق  صور شخصية</p>
                                    <input type="file" id="personal_picture" name="personal_picture"  ">


                                    <p >الرجاء ارفاق نسخة من شهادة الميلاد</p>
                                    <input type="file" id="birth_certificate" name="birth_certificate">
                                </div>

                                <div class="form-holder">

                                   	<p >الرجاء ارفاق نسخة من بطاقة الأب</p>
                                        <input type="file" id="father_card" name="father_card" >



                                    <p>الرجاء ارفاق نخسة من الكرت الصحي </p>
                                    <input type="file" id="medical_card" name="medical_card" multiple>
                                </div>

                            </div> </section>
                    <h2>
                        <p class="step-icon"><span>✓</span></p>
                        <span class="step-text"> لقد انتهينا</span>
                    </h2>
                    <section>
                        <div class="inner" style="    height: 39rem;">
                            <div class="wizard-header">
                                <h3 class="heading"></h3>
                                <p>
شكرا لقيامكم بالتسجيل
                                </p>
                            </div>
                            <div class="form-row">
                                <div class="form-holder form-holder-2">


                                    <div class="plan-total">
                                    <h1 style="text-align:center;">Contact Us</h1>
                                        <p class="plan-text">
                                            Telephone :  24845554
                                        </p>



                                    <div class="plan-total">

                                        <p class="plan-text">
                                         Instagram:  Alrazi_school
                                        </p>


                                        <p class="">Email:  alrazi96@gmail.com</p>


                                        <p class="">
                                  Facebook:  الرازي الرازي
                                        </p>
                                        <p class="">
                                  Twitter:  @alrazi_schoool
                                        </p>

                                </div></div></div>
                            </div>
                        </div>
                    </section>
                </div>

            </form>
        </div>
    </div>
</div>

<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script>
    $(function(){
        let form = $("#form-register");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            rules: {
                confirm: {
                    equalTo: "#password"
                }
            }
        });
        $("#form-total").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            enableAllSteps: true,
            stepsOrientation: "vertical",
            autoFocus: true,
            transitionEffectSpeed: 500,
            titleTemplate : '<div class="title">#title#</div>',
            labels: {
                previous : 'Back Step',
                next : '<i class="zmdi zmdi-arrow-right"></i>',
                finish : '<i class="zmdi zmdi-check"></i>',
                current : ''
            },
            //  onStepChanging: function (event, currentIndex, newIndex)
            // {
            //     x();
            //     // const first_name = document.getElementById('first_name');
            //     // const last_name = document.getElementById('last_name');
            //     // const birth_place = document.getElementById('birth_place');
            //     // const area = document.getElementById('area');
            //     // const civil_number = document.getElementById('civil_number');
            //     // const passport_number = document.getElementById('passport_number');
            //     // const nationality = document.getElementById('nationality');
            //     // const date_of_birth = document.getElementById('date_of_birth');
            //     // // console.log(`${first_name.value}  ${last_name.value}${birth_place.value}  ${area.value} ${civil_number.value}${passport_number.value}  ${nationality.value}${date_of_birth.value}`);
            //     //  checkRequired([first_name,last_name,birth_place,area,civil_number,passport_number,nationality,date_of_birth]) ;
            // },
            onFinished: function (event, currentIndex) {
                formData=new FormData();
                if(!($('#personal_picture').prop('files')[0] === undefined)){
                    formData.append('personal_picture',$('#personal_picture').prop('files')[0]);
                }
                if(!($('#birth_certificate').prop('files')[0] === undefined)){
                    formData.append('birth_certificate',$('#birth_certificate').prop('files')[0]);
                }
                if(!($('#father_card').prop('files')[0] === undefined)){
                    formData.append('father_card',$('#father_card').prop('files')[0]);
                }
                if(!($('#medical_card').prop('files')[0] === undefined)){
                    formData.append('medical_card',$('#medical_card').prop('files')[0]);
                }
                formData.append('first_name',$("#first-name").val());
                formData.append('last_name',$("#last-name").val());
                formData.append('birth_place',$("#birth_place").val());
                formData.append('area',$("#area").val());
                formData.append('civil_number',$("#civil_number").val());
                formData.append('passport_number',$("#passport_number").val());
                formData.append('nationality',$("#nationality").val());
                formData.append('date_of_birth',$("#date_of_birth").val());
                formData.append('issue_date',$("#issue_date").val());
                formData.append('guardian_first_name',$("#guardian_first_name").val());
                formData.append('guardian_last_name',$("#guardian_last_name").val());
                formData.append('guardian_nationality',$("#guardian_nationality").val());
                formData.append('relativity',$("#relativity").val());
                formData.append('post_code',$("#post_code").val());
                formData.append('post_box',$("#post_box").val());
                formData.append('guardian_educational_degree',$("#guardian_educational_degree").val());
                formData.append('address',$("#address").val());
                formData.append('guardian_job',$("#guardian_job").val());
                formData.append('guardian_phone',$("#guardian_phone").val());
                formData.append('mother_first_name',$("#mother_first_name").val());
                formData.append('mother_last_name',$("#mother_last_name").val());
                formData.append('mother_educational_degree',$("#mother_educational_degree").val());
                formData.append('mother_job',$("#mother_job").val());
                formData.append('mother_work_address',$("#mother_work_address").val());
                formData.append('mother_phone',$("#mother_phone").val());
                formData.append('relative_first_name',$("#relative_first_name").val());
                formData.append('relative_last_name',$("#relative_last_name").val());
                formData.append('relative_relativity',$("#relative_relativity").val());
                formData.append('relative_phone',$("#relative_phone").val());
                formData.append('relative_citizen_number',$("#relative_citizen_number").val());
                formData.append('personal_picture',$("#personal_picture").val());
                formData.append('birth_certificate',$("#birth_certificate").val());
                formData.append('father_card',$("#father_card").val());
                formData.append('medical_card',$("#medical_card").val());
                formData.append('family_number',$("#family_number").val());
                formData.append('life_status',$("#life_status").val());
                formData.append('sisters_number',$("#sisters_number").val());
                formData.append('brothers_number',$("#brothers_number").val());
                formData.append('sort_of_family',$("#sort_of_family").val());
                formData.append('num_of_brothers_on_school',$("#num_of_brothers_on_school").val());
                formData.append('stu_live_with',$("#stu_live_with").val());
                formData.append('how_to_go',$("#how_to_go").val());
                formData.append('date_issuance_the_birth_certificate',$('#date_issuance_the_birth_certificate').val());
                formData.append('social_status',$('#social_status').val());
                formData.append('personal_card_number',$('#personal_card_number').val());
                formData.append("_token", "{{ csrf_token() }}");

                $.ajax({
                    url:'/api/store',
                    type:'POST',
                    data: formData,
                    dataType: 'json',
                    contentType: false,
                    processData: false,
                    success:function (response) {

                        console.log(formData);
                       var x=1;
                        Swal.fire (
                            'تم التسجيل بنجاح',
                            `OK الرجاء الضغط على`,
                            'success'
                                  );
                            // location.reload();
                    },
                    error:function (response) {
                        console.log(response.responseText);
                        Swal.fire (
                            'الرجاء التحقق من ان جميع الحقول تحتوي على المعلومات المطلوبة',
              'press ok to continue!',
              'error'
            );
                    }
                });
            }
        })
    });
</script>

</body>
</html>
