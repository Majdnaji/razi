<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('entry', function (Blueprint $table) {
            $table->id();

            $table->string('first_name');
            $table->string('last_name');
            $table->string('birth_place');
            $table->string('area');
            $table->string('passport_number');
            $table->string('nationality');
            $table->string('civil_number');
            $table->timestamp('date_of_birth')->nullable();
            $table->timestamp('issue_date')->nullable();
            $table->string('guardian_first_name');
            $table->string('guardian_last_name');
            $table->string('guardian_nationality');
            $table->string('relativity');
            $table->string('post_code');
            $table->string('post_box');
            $table->string('guardian_educational_degree');
            $table->string('address');
            $table->string('guardian_job');
            $table->string('guardian_phone');
            $table->string('mother_first_name');
            $table->string('mother_last_name');
            $table->string('mother_educational_degree');
            $table->string('mother_job');
            $table->string('mother_work_address');
            $table->string('mother_phone');
            $table->string('relative_first_name');
            $table->string('relative_last_name');
            $table->string('relative_relativity');
            $table->string('relative_phone');
            $table->string('relative_citizen_number');
            $table->string('personal_picture')->nullable();
            $table->string('birth_certificate')->nullable();
            $table->string('father_card')->nullable();
            $table->string('medical_card')->nullable();
            $table->string('family_number');
            $table->string('life_status');
            $table->string('sisters_number');
            $table->string('brothers_number');
            $table->string('sort_of_family');
            $table->string('num_of_brothers_on_school');
            $table->string('stu_live_with');
            $table->string('how_to_go');
            $table->string('date_issuance_the_birth_certificate');
            $table->string('social_status');
            $table->string('personal_card_number');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('entry');
    }
}
