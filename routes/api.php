<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;
use App\Models\Entry;
use niklasravnsborg\LaravelPdf\Facades\Pdf;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/pdf',function (Request $request){
    $data = [
        'first_name' => 'bar'
    ];
    PDF::loadView('pdf', $data, [], [
        'format' => 'A5-L'
    ])->save(public_path('/pdf'));
    return response(['success'=>true])->status(200);
});
Route::post('/store',function (Request $request){
    $validated=$request->validate([
        'first_name'=>'required',
        'last_name'=>'required',
        'birth_place'=>'required',
        'area'=>'required',
        'civil_number'=>'required',
        'passport_number'=>'required',
        'nationality'=>'required',
        'date_of_birth'=>'required',
        'issue_date'=>'required',
        'guardian_first_name'=>'required',
        'guardian_last_name'=>'required',
        'guardian_nationality'=>'required',
        'relativity'=>'required',
        'post_code'=>'required',
        'post_box'=>'required',
        'guardian_educational_degree'=>'required',
        'address'=>'required',
        'guardian_job'=>'required',
        'guardian_phone'=>'required',
        'mother_first_name'=>'required',
        'mother_last_name'=>'required',
        'mother_educational_degree'=>'required',
        'mother_job'=>'required',
        'mother_work_address'=>'required',
        'mother_phone'=>'required',
        'relative_first_name'=>'required',
        'relative_last_name'=>'required',
        'relative_relativity'=>'required',
        'relative_phone'=>'required',
        'relative_citizen_number'=>'required',
        'family_number'=>'required',
        'life_status'=>'required',
        'sisters_number'=>'required',
        'brothers_number'=>'required',
        'sort_of_family'=>'required',
        'num_of_brothers_on_school'=>'required',
        'stu_live_with'=>'required',
        'how_to_go'=>'required',
        'date_issuance_the_birth_certificate'=>'required',
        'social_status'=>'required',
        'personal_card_number'=>'required'

    ]);
    $images=$request->validate([
        'personal_picture'=>'required|image|max:2048',
        'birth_certificate'=>'required|image|max:2048',
        'father_card'=>'required|image|max:2048',
        'medical_card'=>'required|image|max:2048'
    ]);
    $entry=new Entry();
    $entry->fill($validated);
    $entry->save();
    if($request->hasFile('personal_picture')){
        $image=$request->file('personal_picture');
        $extension=$image->getClientOriginalExtension();
        $name=$entry->id.".".$extension;
        $destinationPath = public_path('/images/personal');
        $image->move($destinationPath, $name);
        $entry->personal_picture=$name;

    }
    if($request->hasFile('birth_certificate')){
        $image=$request->file('birth_certificate');
        $extension=$image->getClientOriginalExtension();
        $name=$entry->id.".".$extension;
        $destinationPath = public_path('/images/birth');
        $image->move($destinationPath, $name);
        $entry->birth_certificate=$name;
    }
    if($request->hasFile('father_card')){
        $image=$request->file('father_card');
        $extension=$image->getClientOriginalExtension();
        $name=$entry->id.".".$extension;
        $destinationPath = public_path('/images/father_card');
        $image->move($destinationPath, $name);
        $entry->father_card=$name;
    }
    if($request->hasFile('medical_card')){
        $image=$request->file('medical_card');
        $extension=$image->getClientOriginalExtension();
        $name=$entry->id.".".$extension;
        $destinationPath = public_path('/images/medical_card');
        $image->move($destinationPath, $name);
        $entry->medical_card=$name;
    }
    $entry->save();
    $view= view()->share('entry',$entry);
    $pdf = PDF::loadView('pdf', $entry);
    \Illuminate\Support\Facades\Storage::put('public/pdf/invoice.pdf', $pdf->output());
    return response(['success'=>true])->status(200);
});

